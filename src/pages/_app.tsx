import React from 'react';
import { AppProps } from 'next/dist/next-server/lib/router/router';
import { Helmet } from 'react-helmet';

export interface PageWithTitle {
  title: string;
}

const APP_NAME = 'next-pwa-example';
const APP_DESCRIPTION = 'This is an example of using next-pwa plugin';

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <>
      <Helmet
        htmlAttributes={{ lang: 'en', dir: 'ltr' }}
        title={pageProps.title || APP_NAME}
        meta={[
          { name: 'application-name', content: APP_NAME },
          { name: 'description', content: APP_DESCRIPTION },
          { name: 'mobile-web-app-capable', content: 'yes' },
          { name: 'apple-mobile-web-app-title', content: APP_NAME },
          { name: 'apple-mobile-web-app-capable', content: 'yes' },
          {
            name: 'apple-mobile-web-app-status-bar-style',
            content: 'default',
          },
          { name: 'format-detection', content: 'telephone=no' },
          { name: 'theme-color', content: '#FFFFFF' },
          /* TIP: set viewport head meta tag in _app.js, otherwise it will show a warning */
          {
            name: 'viewport',
            content:
              'minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no, viewport-fit=cover',
          },
        ]}
        link={[
          {
            rel: 'apple-touch-icon',
            sizes: '180x180',
            href: '/icons/apple-touch-icon.png',
          },
          { rel: 'manifest', href: '/manifest.json' },
          { rel: 'shortcut icon', href: '/icons/favicon.ico' },
        ]}
      />
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
