import { GetStaticProps } from 'next';
import React from 'react';
import { PageWithTitle } from './_app';

const Index = (): JSX.Element => <h1>Next.js + PWA = AWESOME!</h1>;

export const getStaticProps: GetStaticProps<PageWithTitle> = async () => {
  return {
    props: {
      title: 'next-pwa-index',
    },
  };
};

export default Index;
